package com.success.android.popularmovies2.presenters;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.success.android.popularmovies2.GlideRequests;
import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.fragments.DetailsFragment;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.utilities.DateUtils;
import com.success.android.popularmovies2.views.BaseView;
import com.success.android.popularmovies2.views.details.DetailsViewImpl;

import java.security.InvalidParameterException;

public class DetailsPresenter implements BasePresenter {

    @Nullable
    private DetailsViewImpl detailsView;
    private MovieDataManager dataManager;
    private GlideRequests glide;

    private final Movie movie;

    public DetailsPresenter(MovieDataManager dataManager,
                            GlideRequests glide,
                            Bundle savedState) {
        this.dataManager = dataManager;
        this.glide = glide;
        if (savedState.containsKey(DetailsFragment.KEY_MOVIE)) {
            movie = savedState.getParcelable(DetailsFragment.KEY_MOVIE);
        } else {
            throw new InvalidParameterException("The savedState must contains a Movie as Parcelable");
        }
    }

    @Override
    public void attachView(BaseView view) {
        try {
            this.detailsView = (DetailsViewImpl) view;
        } catch (ClassCastException e) {
            throw new ClassCastException("You must attach a DetailsView to DetailsPresenter");
        }
        if (hasDetailsView()) {
            detailsView.setTitle(movie.getTitle());
            detailsView.setReleaseDate(DateUtils.getDateFromTimeInMillis(movie.getReleaseDate()));
            detailsView.setPlotSynopsis(movie.getPlotSynopsis());
            detailsView.setVoteAverage(String.valueOf(movie.getVoteAverage()));
            glide.load(movie.getPosterPath())
                    .placeholder(R.drawable.movie_poster_placeholder)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            detailsView.setBackground(resource);
                        }
                    });
        }
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putParcelable(DetailsFragment.KEY_MOVIE, movie);
        return state;
    }

    private boolean hasDetailsView() {
        return detailsView != null;
    }
}