package com.success.android.popularmovies2.views;

import android.os.Bundle;
import android.view.View;

/**
 * Created by Noki 2 on 2018. 03. 16..
 */

public interface BaseView {

    View getRootView();

    Bundle getViewState();
}
