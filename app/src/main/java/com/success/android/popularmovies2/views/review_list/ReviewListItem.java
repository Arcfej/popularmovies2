package com.success.android.popularmovies2.views.review_list;

public interface ReviewListItem {

    void setReview(String review);
}
