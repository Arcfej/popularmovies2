package com.success.android.popularmovies2.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Noki 2 on 2018. 03. 18..
 */

public class DateUtils {

    private DateUtils() {

    }

    public static long getTimeInMillisFromThemoviedbResponse(String dateString) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = format.parse(dateString);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static String getDateFromTimeInMillis(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy. MM. dd.");
        return formatter.format(new Date(date));
    }
}