package com.success.android.popularmovies2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.popularmovies2.GlideApp;
import com.success.android.popularmovies2.MainActivity;
import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.presenters.DetailsPresenter;
import com.success.android.popularmovies2.views.details.DetailsViewImpl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class DetailsFragment extends Fragment {

    private static final String KEY_PRESENTER_STATE = "presenter_state";
    public static final String KEY_MOVIE = "movie";

    private static final String TAG_TRAILER_LIST_FRAGMENT = "trailers";
    private static final String TAG_REVIEW_LIST_FRAGMENT = "reviews";

    private Bundle presenterState;

    private Movie movie;
    private boolean isFavourite;

    private DetailsPresenter presenter;
    private MovieDataManager dataManager;
    private DetailsViewImpl view;
    private Fragment reviewList;
    private Fragment trailerList;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataManager = MovieDataManagerImpl.getInstance(context.getContentResolver(), getResources());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments == null || !arguments.containsKey(KEY_MOVIE)) {
            throw new IllegalArgumentException("The DetailsFragment must contain a Movie in it's arguments");
        } else {
            movie = arguments.getParcelable(KEY_MOVIE);
        }

        Bundle nestedArguments = new Bundle();
        nestedArguments.putParcelable(KEY_MOVIE, movie);

        reviewList = getChildFragmentManager().findFragmentByTag(TAG_REVIEW_LIST_FRAGMENT);
        if (reviewList == null) {
            reviewList = new ReviewListFragment();
            reviewList.setArguments(nestedArguments);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fl_review_list, reviewList, TAG_REVIEW_LIST_FRAGMENT)
                    .commit();
        }

        trailerList = getChildFragmentManager().findFragmentByTag(TAG_TRAILER_LIST_FRAGMENT);
        if (trailerList == null) {
            trailerList = new TrailerListFragment();
            trailerList.setArguments(nestedArguments);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fl_trailer_list, trailerList, TAG_TRAILER_LIST_FRAGMENT)
                    .commit();
        }

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_PRESENTER_STATE)) {
            presenterState = savedInstanceState.getBundle(KEY_PRESENTER_STATE);
        } else {
            presenterState = new Bundle();
            presenterState.putParcelable(KEY_MOVIE, movie);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new DetailsPresenter(
                dataManager,
                GlideApp.with(this),
                presenterState
        );
        view = new DetailsViewImpl(getLayoutInflater(), container, getResources());
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        dataManager.isFavourite(movie.getMovieId());
        EventBus.getDefault().post(new MainActivity.TitleChangedEvent(movie.getTitle()));
        presenter.attachView(view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.add_remove_favourite);
        if (isFavourite) {
            item.setIcon(R.drawable.star_full);
            item.setTitle(R.string.menu_item_remove_favourite);
        } else {
            item.setIcon(R.drawable.star_empty);
            item.setTitle(R.string.menu_item_add_favourite);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_remove_favourite) {
            isFavourite = !isFavourite;
            if (isFavourite) {
                dataManager.addMovieToFavourite(movie);
            } else {
                dataManager.removeMovieFromFavourites(movie.getMovieId());
            }
            if (getActivity() != null) {
                getActivity().invalidateOptionsMenu();
            }
            return true;
        } else return false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(KEY_PRESENTER_STATE, presenter.getState());
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Subscribe
    public void onFavouriteStateChanged(MovieDataManagerImpl.MovieFavouriteStateChanged event) {
        if (event.movieId == movie.getMovieId()) {
            isFavourite = event.isFavourite;
            if (getActivity() != null) {
                getActivity().invalidateOptionsMenu();
            }
        }
    }
}