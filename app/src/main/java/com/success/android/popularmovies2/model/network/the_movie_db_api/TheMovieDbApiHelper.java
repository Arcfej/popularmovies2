package com.success.android.popularmovies2.model.network.the_movie_db_api;

public interface TheMovieDbApiHelper {

    void downloadMostPopularMoviesPage(int page);

    void downloadHighestRatedMoviesPage(int page);

    void downloadReviewsPage(int movieId, int page);

    void downloadTrailers(int movieId);
}
