package com.success.android.popularmovies2.views.trailer_list;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.views.dropdown_list.DropdownListView;

public class TrailerListView extends DropdownListView {

    public TrailerListView(Context applicationContext, LayoutInflater inflater, ViewGroup container,
                           RecyclerView.Adapter presenter, Bundle viewState) {
        super(applicationContext, inflater, container, presenter, viewState,
                applicationContext.getResources().getString(R.string.trailers_head_title),
                LinearLayoutManager.HORIZONTAL);
    }
}
