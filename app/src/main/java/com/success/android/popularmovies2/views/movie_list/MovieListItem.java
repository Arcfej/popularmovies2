package com.success.android.popularmovies2.views.movie_list;

import android.net.Uri;

import com.success.android.popularmovies2.GlideRequests;
import com.success.android.popularmovies2.pojos.Movie;

public interface MovieListItem {

    void setPoster(GlideRequests glideRequest, Uri posterPath);

    void setOnClickListener(Movie movie);
}
