package com.success.android.popularmovies2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.success.android.popularmovies2.GlideApp;
import com.success.android.popularmovies2.MainActivity;
import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.presenters.PosterAdapter;
import com.success.android.popularmovies2.views.movie_list.MovieListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MovieListFragment extends Fragment {

    private static final String KEY_PRESENTER_STATE = "presenter_state";

    private Bundle presenterState;

    private PosterAdapter presenter;
    private MovieDataManager dataManager;
    private MovieListView view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataManager = MovieDataManagerImpl.getInstance(context.getContentResolver(), getResources());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_PRESENTER_STATE)) {
            presenterState = savedInstanceState.getBundle(KEY_PRESENTER_STATE);
        }
        requireActivity().setTitle(R.string.activity_title_most_popular);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new PosterAdapter(dataManager, GlideApp.with(this), presenterState);

        view = new MovieListView(inflater, container, presenter,
                (WindowManager) requireActivity().getSystemService(Context.WINDOW_SERVICE));

        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        EventBus.getDefault().register(this);
        EventBus.getDefault().register(presenter);
        presenter.attachView(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        String newTitle = getTitleFromListType(presenter.getCurrentListType());
        EventBus.getDefault().post(new MainActivity.TitleChangedEvent(newTitle));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_movie_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MovieDataManager.ListType newListType;
        switch (item.getItemId()) {
            case R.id.sort_highest_rated:
                newListType = MovieDataManager.ListType.TOP_RATED;
                break;
            case R.id.sort_most_popular:
                newListType = MovieDataManager.ListType.MOST_POPULAR;
                break;
            case R.id.favourites:
                newListType = MovieDataManager.ListType.FAVOURITE;
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        EventBus.getDefault().post(new MovieDataManagerImpl.ListTypeChangedEvent(newListType));
        return true;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(KEY_PRESENTER_STATE, presenter.getState());
    }

    @Override
    public void onPause() {
        super.onPause();
        presenterState = presenter.getState();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        EventBus.getDefault().unregister(presenter);
    }

    @Subscribe
    public void onListTypeChanged(MovieDataManagerImpl.ListTypeChangedEvent event) {
        String newTitle = getTitleFromListType(event.newListType);
        EventBus.getDefault().post(new MainActivity.TitleChangedEvent(newTitle));
    }

    private String getTitleFromListType(MovieDataManager.ListType listType) {
        switch (listType) {
            case MOST_POPULAR:
                return getString(R.string.activity_title_most_popular);
            case TOP_RATED:
                return getString(R.string.activity_title_highest_rated);
            case FAVOURITE:
                return getString(R.string.activity_title_favourites);
            default:
                return null;
        }
    }
}