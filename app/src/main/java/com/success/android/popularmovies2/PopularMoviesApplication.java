package com.success.android.popularmovies2;

import android.app.Application;

import com.example.myapp.MyEventBusIndex;

import org.greenrobot.eventbus.EventBus;

public class PopularMoviesApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Default EventBus for the presenters
        EventBus.builder().addIndex(new MyEventBusIndex()).installDefaultEventBus();
    }
}
