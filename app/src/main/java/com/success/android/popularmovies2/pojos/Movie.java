package com.success.android.popularmovies2.pojos;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Noki 2 on 2018. 03. 16..
 */

public class Movie implements Parcelable {

    private final int movieId;
    private final String title;
    private final String plotSynopsis;
    private final long releaseDate;
    private final Uri posterPath;
    private final double voteAverage;


    public Movie(int movieId, String title, String plotSynopsis, long releaseDate,
                 Uri posterPath, double voteAverage) {
        this.movieId = movieId;
        this.title = title;
        this.plotSynopsis = plotSynopsis;
        this.releaseDate = releaseDate;
        this. posterPath = posterPath;
        this. voteAverage = voteAverage;
    }


    private Movie(Parcel in) {
        movieId = in.readInt();
        title = in.readString();
        plotSynopsis = in.readString();
        releaseDate = in.readLong();
        posterPath = Uri.parse(in.readString());
        voteAverage = in.readDouble();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(movieId);
        dest.writeString(title);
        dest.writeString(plotSynopsis);
        dest.writeLong(releaseDate);
        dest.writeString(posterPath.toString());
        dest.writeDouble(voteAverage);
    }

    public int getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getPlotSynopsis() {
        return plotSynopsis;
    }

    public long getReleaseDate() {
        return releaseDate;
    }

    public Uri getPosterPath() {
        return posterPath;
    }

    public double getVoteAverage() {
        return voteAverage;
    }
}
