package com.success.android.popularmovies2.views.review_list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.success.android.popularmovies2.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewHolder extends RecyclerView.ViewHolder implements ReviewListItem {

    @BindView(R.id.tv_review)
    TextView review;

    private ReviewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static ReviewHolder constructWithInflating(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_review, parent, false);
        return new ReviewHolder(itemView);
    }

    @Override
    public void setReview(@NonNull String review) {
        this.review.setText(review);
    }
}
