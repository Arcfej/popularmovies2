package com.success.android.popularmovies2.model.database;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import org.greenrobot.eventbus.EventBus;

public class MoviesDbCrudHandler extends AsyncQueryHandler {

    private EventBus dataManagerBus;

    public MoviesDbCrudHandler(ContentResolver cr, EventBus dataManagerBus) {
        super(cr);
        this.dataManagerBus = dataManagerBus;
    }

    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        dataManagerBus.post(new QueryCompletedEvent(token, cursor));
    }

    @Override
    protected void onInsertComplete(int token, Object cookie, Uri uri) {
        dataManagerBus.post(new InsertionCompletedEvent(token, uri));
    }

    @Override
    protected void onUpdateComplete(int token, Object cookie, int result) {

    }

    @Override
    protected void onDeleteComplete(int token, Object cookie, int result) {
        dataManagerBus.post(new DeletionCompletedEvent(token, result));
    }

    public class QueryCompletedEvent {

        public final int token;
        public final Cursor cursor;

        public QueryCompletedEvent(int token, Cursor cursor) {
            this.token = token;
            this.cursor = cursor;
        }
    }

    public class InsertionCompletedEvent {

        public final int token;
        public final Uri uri;

        public InsertionCompletedEvent(int token, Uri uri) {
            this.token = token;
            this.uri = uri;
        }
    }

    public class DeletionCompletedEvent {

        public final int token;
        public final int deletedRows;

        public DeletionCompletedEvent(int token, int deletedRows) {
            this.token = token;
            this.deletedRows = deletedRows;
        }
    }
}