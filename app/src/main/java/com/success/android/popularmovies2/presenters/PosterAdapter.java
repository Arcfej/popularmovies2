package com.success.android.popularmovies2.presenters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.success.android.popularmovies2.GlideRequests;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.views.BaseView;
import com.success.android.popularmovies2.views.movie_list.MovieListView;
import com.success.android.popularmovies2.views.movie_list.MoviePosterHolder;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class PosterAdapter extends RecyclerView.Adapter<MoviePosterHolder> implements BasePresenter {

    private static final String KEY_CURRENT_LIST_TYPE = "list_type";

    @Nullable
    private MovieListView listView;
    private MovieDataManager dataManager;
    private GlideRequests glide;

    private MovieDataManager.ListType currentListType = MovieDataManager.ListType.MOST_POPULAR;
    private int currentListSize = 0;
    private ArrayList<Movie> currentMovieList = new ArrayList<>(0);

    public PosterAdapter(MovieDataManager dataManager, GlideRequests glide, Bundle savedState) {
        if (savedState != null && savedState.containsKey(KEY_CURRENT_LIST_TYPE)) {
            currentListType = MovieDataManager.ListType.valueOf(savedState.getString(KEY_CURRENT_LIST_TYPE));
        }
        this.dataManager = dataManager;
        this.glide = glide;
    }

    @Override
    public void attachView(BaseView view) {
        try {
            this.listView = (MovieListView) view;
        } catch (ClassCastException e) {
            throw new ClassCastException("You must attach a MovieListView to PosterAdapter");
        }

        dataManager.requestMovieList(currentListType);
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putString(KEY_CURRENT_LIST_TYPE, currentListType.name());
        return state;
    }

    public MovieDataManager.ListType getCurrentListType() {
        return currentListType;
    }

    private boolean hasListView() {
        return listView != null;
    }

    @NonNull
    @Override
    public MoviePosterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (!hasListView()) {
            throw new NullPointerException("You must attach a view to the presenter");
        }
        return MoviePosterHolder.constructWithInflating(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull final MoviePosterHolder holder, int position) {
        Movie movie = currentMovieList.get(position);
        if (movie != null) {
            holder.setPoster(glide, movie.getPosterPath());
            holder.setOnClickListener(movie);
        }
    }

    @Override
    public int getItemCount() {
        return currentListSize;
    }

    @Subscribe
    public void onMovieReady(MovieDataManagerImpl.MovieReadyEvent event) {
        if (currentListType == event.listType) {
            if (event.positionInList >= currentListSize) {
                currentMovieList.ensureCapacity(event.positionInList + 1);
                currentMovieList.add(event.positionInList, event.movie);
                int previousListSize = currentListSize;
                currentListSize = event.positionInList + 1;
                notifyItemRangeInserted(currentListSize, event.positionInList - previousListSize + 1);
            } else {
                currentMovieList.add(event.positionInList, event.movie);
                notifyItemChanged(event.positionInList);
            }
        }
    }

    @Subscribe
    public void onListSizeReady(MovieDataManagerImpl.ListSizeReadyEvent event) {
        if (currentListType == event.listType) {
            int previousListSize = currentListSize;
            currentListSize = event.size;
            currentMovieList.ensureCapacity(currentListSize);
            notifyItemRangeInserted(previousListSize, currentListSize - previousListSize);
        }
    }

    @Subscribe
    public void onListTypeChanged(MovieDataManagerImpl.ListTypeChangedEvent event) {
        if (event.newListType != currentListType) {
            currentListType = event.newListType;
            currentListSize = 0;
            currentMovieList.clear();
            dataManager.requestMovieList(event.newListType);
            notifyDataSetChanged();
        }
    }
}