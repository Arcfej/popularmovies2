package com.success.android.popularmovies2.presenters;

import android.os.Bundle;

import com.success.android.popularmovies2.views.BaseView;

public interface BasePresenter {

    /**
     * Must call in onStart()
     * @param view
     */
    void attachView(BaseView view);

    Bundle getState();
}
