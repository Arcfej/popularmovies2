package com.success.android.popularmovies2.model.network;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import java.util.Map;

/**
 * Created by Noki 2 on 2018. 03. 19..
 */

public class NetworkHelper {

    private NetworkHelper() {
        throw new AssertionError("You shouldn't have any instance of NetworkHelper");
    }

    public static void startDownloadJSONObject(@NonNull Uri url,
                                               @Nullable Map<String, String> query,
                                               @Nullable String tag,
                                               @NonNull JSONObjectRequestListener listener) {
        ANRequest.GetRequestBuilder builder = AndroidNetworking.get(url.toString());
        if (query != null) {
            builder.addQueryParameter(query);
        }
        if (!TextUtils.isEmpty(tag)) {
            builder.setTag(tag);
        }
        builder.setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(listener);
    }

    public static void prefetchJSONObject(@NonNull Uri url,
                                          @Nullable Map<String, String> query,
                                          @Nullable String tag) {
        ANRequest.GetRequestBuilder builder = AndroidNetworking.get(url.toString());
        if (query != null) {
            builder.addQueryParameter(query);
        }
        if (!TextUtils.isEmpty(tag)) {
            builder.setTag(tag);
        }
        builder.setPriority(Priority.MEDIUM)
                .build()
                .prefetch();
    }

    public static void cancelDownload(String tag) {
        AndroidNetworking.cancel(tag);
    }
}
