package com.success.android.popularmovies2.utilities;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created by Noki 2 on 2018. 03. 23..
 */

public class FormatHelper {

    private FormatHelper() {

    }

    public static String roundDoubleToOneDigit(double number) {
        DecimalFormat formatter = new DecimalFormat("#.#");
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        return formatter.format(number);
    }
}
