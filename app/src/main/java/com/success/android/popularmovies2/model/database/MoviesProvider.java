package com.success.android.popularmovies2.model.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by Noki 2 on 2018. 03. 26..
 */

public class MoviesProvider extends ContentProvider {

    private static final int FAVOURITE_MOVIES = 100;
    private static final int FAVOURITE_MOVIE_ID = 101;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(MoviesDbContract.CONTENT_AUTHORITY,
                MoviesDbContract.PATH_FAVOURITE_MOVIES,
                FAVOURITE_MOVIES);
        uriMatcher.addURI(MoviesDbContract.CONTENT_AUTHORITY,
                MoviesDbContract.PATH_FAVOURITE_MOVIES + "/#",
                FAVOURITE_MOVIE_ID);
    }

    private MoviesDbOpenHelper dbOpenHelper;

    @Override
    public boolean onCreate() {
        dbOpenHelper = new MoviesDbOpenHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Bundle call(@NonNull String method, @Nullable String arg, @Nullable Bundle extras) {
        Bundle bundle = new Bundle();
        switch (method) {
            case MoviesDbContract.FavouriteMoviesEntry.CALL_FAVOURITE_MOVIES_COUNT:
                SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
                SQLiteStatement statement = db.compileStatement(
                        "SELECT COUNT(*) FROM " + MoviesDbContract.FavouriteMoviesEntry.TABLE_NAME);
                bundle.putLong(MoviesDbContract.FavouriteMoviesEntry.CALL_FAVOURITE_MOVIES_COUNT,
                        statement.simpleQueryForLong());
        }
        return bundle;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();

        Cursor cursor;

        switch (uriMatcher.match(uri)) {
            case FAVOURITE_MOVIES:
                cursor = db.query(MoviesDbContract.FavouriteMoviesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case FAVOURITE_MOVIE_ID:
                selection = MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(MoviesDbContract.FavouriteMoviesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Wrong uri to query!");
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        switch (uriMatcher.match(uri)) {
            case FAVOURITE_MOVIES:
                if (values != null) {
                    return insertMovie(uri, values);
                }
            default:
                throw new IllegalArgumentException("Not supported insertion: " + uri);
        }
    }

    private Uri insertMovie(Uri uri, ContentValues values) {
        Integer movieId = values.getAsInteger(MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID);
        if (movieId == null) {
            throw new IllegalArgumentException("The movie must have an id!");
        }

        String title = values.getAsString(MoviesDbContract.FavouriteMoviesEntry.COLUMN_TITLE);
        if (TextUtils.isEmpty(title)) {
            throw new IllegalArgumentException("The movie must have a title");
        }

        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        long insertedRowId = db.insert(MoviesDbContract.FavouriteMoviesEntry.TABLE_NAME,
                null, values);
        if (insertedRowId == -1) {
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, insertedRowId);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[]
            selectionArgs) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int deletedMoviesCount;
        switch (uriMatcher.match(uri)) {
            case FAVOURITE_MOVIE_ID:
                selection = MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                deletedMoviesCount = db.delete(MoviesDbContract.FavouriteMoviesEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Not supported delete operation for: " + uri);
        }

        if (deletedMoviesCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deletedMoviesCount;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String
            selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case FAVOURITE_MOVIES:
                return MoviesDbContract.FavouriteMoviesEntry.CONTENT_LIST_TYPE;
            case FAVOURITE_MOVIE_ID:
                return MoviesDbContract.FavouriteMoviesEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("No match for the URI: " + uri);
        }
    }
}