package com.success.android.popularmovies2.views.trailer_list;

import android.net.Uri;

import com.success.android.popularmovies2.GlideRequests;

public interface TrailerListItem {

    void setTrailerThumb(GlideRequests glideRequest, Uri thumbPath);

    void setOnClickListener(Uri watchUri);
}
