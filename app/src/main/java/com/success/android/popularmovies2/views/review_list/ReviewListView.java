package com.success.android.popularmovies2.views.review_list;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.presenters.ReviewAdapter;
import com.success.android.popularmovies2.views.dropdown_list.DropdownListView;

public class ReviewListView extends DropdownListView {

    public ReviewListView(Context applicationContext, LayoutInflater inflater, ViewGroup container,
                          ReviewAdapter presenter, Bundle viewState) {
        super(applicationContext, inflater, container, presenter, viewState,
                applicationContext.getResources().getString(R.string.reviews_head_title), LinearLayoutManager.VERTICAL);
    }
}
