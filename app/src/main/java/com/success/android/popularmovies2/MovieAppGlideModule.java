package com.success.android.popularmovies2;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class MovieAppGlideModule extends AppGlideModule {}