package com.success.android.popularmovies2.model;

import com.success.android.popularmovies2.pojos.Movie;

public interface MovieDataManager {
    enum ListType {
        MOST_POPULAR,
        TOP_RATED,
        FAVOURITE,
        REVIEW,
        TRAILER
    }

    void subscribeToEvents();
    void unsubscribeFromEvents();

    void requestMovieList(ListType listType);
    void requestReviewList(int movieId);
    void requestTrailerList(int movieId);
    void isFavourite(int movieId);

    void addMovieToFavourite(Movie movie);
    void removeMovieFromFavourites(int movieId);
}
