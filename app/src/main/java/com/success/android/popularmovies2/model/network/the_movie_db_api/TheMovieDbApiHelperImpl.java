package com.success.android.popularmovies2.model.network.the_movie_db_api;

import android.net.Uri;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.network.NetworkHelper;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.pojos.Review;
import com.success.android.popularmovies2.pojos.Trailer;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TheMovieDbApiHelperImpl implements TheMovieDbApiHelper {

    private static final String DOWNLOAD_MOVIE_LIST_TAG = "download_movies";
    private static final String DOWNLOAD_REVIEW_LIST_TAG = "download_movies";
    private static final String DOWNLOAD_TRAILER_LIST_TAG = "prefetch_movies";

    private final String API_KEY;

    private static TheMovieDbApiHelperImpl INSTANCE = null;

    private final EventBus dataManagerBus;

    private TheMovieDbApiHelperImpl(String apiKey, EventBus dataManagerBus) {
        API_KEY = apiKey;
        this.dataManagerBus = dataManagerBus;
    }

    public static synchronized TheMovieDbApiHelperImpl getInstance(String apiKey, EventBus modelEventBus) {
        if (INSTANCE == null) {
            INSTANCE = new TheMovieDbApiHelperImpl(apiKey, modelEventBus);
        }
        return INSTANCE;
    }


    @Override
    public void downloadMostPopularMoviesPage(int page) {
        Uri uri = TheMovieDbApiQueryContract.MovieEntry.buildMostPopularMoviesPathUri();
        JSONObjectRequestListener listener = new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                List<Movie> movieList = TheMovieDbJsonParser.parseMoviesPage(response);
                int pageNumber = TheMovieDbJsonParser.fetchMoviesPageNumber(response);
                dataManagerBus.postSticky(new MoviePageDownloadedEvent(
                        MovieDataManager.ListType.MOST_POPULAR,
                        movieList,
                        pageNumber
                ));
            }

            @Override
            public void onError(ANError anError) {
                // TODO implement
            }
        };
        downloadMoviesPage(uri, page, listener);
    }

    @Override
    public void downloadHighestRatedMoviesPage(int page) {
        Uri uri = TheMovieDbApiQueryContract.MovieEntry.buildHighestRatedMoviesPathUri();
        JSONObjectRequestListener listener = new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                List<Movie> movieList = TheMovieDbJsonParser.parseMoviesPage(response);
                int pageNumber = TheMovieDbJsonParser.fetchMoviesPageNumber(response);
                dataManagerBus.postSticky(new MoviePageDownloadedEvent(
                        MovieDataManager.ListType.TOP_RATED,
                        movieList,
                        pageNumber
                ));
            }

            @Override
            public void onError(ANError anError) {
                // TODO implement
            }
        };
        downloadMoviesPage(uri, page, listener);
    }

    private void downloadMoviesPage(Uri uri, int page, JSONObjectRequestListener listener) {
        Map<String, String> query = new HashMap<>();
        query.put(TheMovieDbApiQueryContract.QUERY_API_KEY, API_KEY);
        query.put(TheMovieDbApiQueryContract.MovieEntry.QUERY_PAGE, String.valueOf(page));
        NetworkHelper.startDownloadJSONObject(
                uri,
                query,
                DOWNLOAD_MOVIE_LIST_TAG + page,
                listener
        );
    }

    @Override
    public void downloadReviewsPage(final int movieId, final int page) {
        Map<String, String> query = new HashMap<>();
        query.put(TheMovieDbApiQueryContract.QUERY_API_KEY, API_KEY);
        query.put(TheMovieDbApiQueryContract.MovieEntry.QUERY_PAGE, String.valueOf(page));
        final Uri uri = TheMovieDbApiQueryContract.ReviewsEntry.buildReviewsDownloadUri(movieId);
        JSONObjectRequestListener listener = new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                List<Review> reviewList = TheMovieDbJsonParser.parseReviewsPage(response);
                int pageNumber = TheMovieDbJsonParser.fetchReviewsPageNumber(response);
                dataManagerBus.postSticky(new ReviewPageDownloadedEvent(
                        reviewList,
                        movieId,
                        pageNumber)
                );
            }

            @Override
            public void onError(ANError anError) {
                // TODO implement
            }
        };
        NetworkHelper.startDownloadJSONObject(uri, query,
                DOWNLOAD_REVIEW_LIST_TAG + page, listener);
    }

    @Override
    public void downloadTrailers(final int movieId) {
        Map<String, String> query = new HashMap<>();
        query.put(TheMovieDbApiQueryContract.QUERY_API_KEY, API_KEY);
        final Uri uri = TheMovieDbApiQueryContract.VideosEntry.buildVideosDownloadUri(movieId);
        JSONObjectRequestListener listener = new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                List<Trailer> trailerList = TheMovieDbJsonParser.parseTrailers(response);
                dataManagerBus.postSticky(new TrailersDownloadedEvent(trailerList, movieId));
            }

            @Override
            public void onError(ANError anError) {
                // TODO implement
            }
        };
        NetworkHelper.startDownloadJSONObject(uri, query,
                DOWNLOAD_TRAILER_LIST_TAG, listener);
    }

    public class MoviePageDownloadedEvent {

        public final MovieDataManager.ListType listType;
        public final List<Movie> movieList;
        public final int pageNumber;

        public MoviePageDownloadedEvent(MovieDataManager.ListType listType,
                                        List<Movie> movieList,
                                        int pageNumber) {
            this.listType = listType;
            this.movieList = movieList;
            this.pageNumber = pageNumber;
        }
    }

    public class ReviewPageDownloadedEvent {

        public final List<Review> reviewList;
        public final int movieId;
        public final int pageNumber;

        public ReviewPageDownloadedEvent(List<Review> reviewList, int movieId, int pageNumber) {
            this.reviewList = reviewList;
            this.movieId = movieId;
            this.pageNumber = pageNumber;
        }
    }

    public class TrailersDownloadedEvent {

        public final List<Trailer> trailerList;
        public final int movieId;

        public TrailersDownloadedEvent(List<Trailer> trailerList, int movieId) {
            this.trailerList = trailerList;
            this.movieId = movieId;
        }
    }

    public class DownloadErrorEvent {

    }
}