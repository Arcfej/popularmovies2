package com.success.android.popularmovies2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.success.android.popularmovies2.fragments.DetailsFragment;
import com.success.android.popularmovies2.fragments.MovieListFragment;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.views.movie_list.MoviePosterHolder;
import com.success.android.popularmovies2.views.trailer_list.TrailerHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_FRAGMENT_MOVIE_LIST = "movie_list";
    private static final String TAG_FRAGMENT_DETAILS = "details";

    private Fragment movieList;
    private Fragment details;

    private MovieDataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Networking library
        AndroidNetworking.initialize(getApplicationContext());

        dataManager = MovieDataManagerImpl.getInstance(getContentResolver(), getResources());

        movieList = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_MOVIE_LIST);
        if (movieList == null) {
            movieList = new MovieListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.root_view, movieList, TAG_FRAGMENT_MOVIE_LIST)
                    .commit();
        }

        details = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_DETAILS);
        if (details == null) {
            details = new DetailsFragment();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        dataManager.subscribeToEvents();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        dataManager.unsubscribeFromEvents();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onListItemClick(MoviePosterHolder.PosterClickEvent event) {
        Bundle arguments = new Bundle();
        arguments.putParcelable(DetailsFragment.KEY_MOVIE, event.movie);
        details.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .addToBackStack(TAG_FRAGMENT_DETAILS)
                .replace(R.id.root_view, details, TAG_FRAGMENT_DETAILS)
                .commit();
    }

    @Subscribe
    public void onTrailerClicked(TrailerHolder.TrailerClickEvent event) {
        Intent intent = new Intent(Intent.ACTION_VIEW, event.watchUri);
        startActivity(intent);
    }

    @Subscribe
    public void onTitleChanged(TitleChangedEvent event) {
        setTitle(event.newTitle);
    }

    public static class TitleChangedEvent {

        @NonNull
        public final String newTitle;

        public TitleChangedEvent(String newTitle) {
            this.newTitle = newTitle;
        }
    }
}