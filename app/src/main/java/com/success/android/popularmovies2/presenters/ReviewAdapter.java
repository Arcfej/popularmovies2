package com.success.android.popularmovies2.presenters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.pojos.Review;
import com.success.android.popularmovies2.views.BaseView;
import com.success.android.popularmovies2.views.dropdown_list.DropdownListView;
import com.success.android.popularmovies2.views.review_list.ReviewHolder;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewHolder> implements BasePresenter {

    @Nullable
    private DropdownListView dropdownListView;
    private MovieDataManager dataManager;

    private final int movieId;
    private ArrayList<Review> reviewList = new ArrayList<>(0);

    public ReviewAdapter(MovieDataManager dataManager, int movieId) {
        this.dataManager = dataManager;
        this.movieId = movieId;
    }

    @Override
    public void attachView(BaseView view) {
        try {
            this.dropdownListView = (DropdownListView) view;
        } catch (ClassCastException e) {
            throw new ClassCastException("You must attach a DropdownListView to ReviewAdapter");
        }

        dataManager.requestReviewList(movieId);
    }

    @Override
    public Bundle getState() {
        return null;
    }

    private boolean hasListView() {
        return dropdownListView != null;
    }

    @NonNull
    @Override
    public ReviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (!hasListView()) {
            throw new NullPointerException("You must attach a view to the presenter");
        }
        return ReviewHolder.constructWithInflating(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewHolder holder, int position) {
        Review review = reviewList.get(position);
        if (review != null) {
            holder.setReview(review.getContent());
        }
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    @Subscribe
    public void onReviewReady(MovieDataManagerImpl.ReviewReadyEvent event) {
        if (movieId == event.movieId) {
            if (event.positionInList >= reviewList.size()) {
                int previousListSize = reviewList.size();
                reviewList.ensureCapacity(event.positionInList + 1);
                reviewList.add(event.positionInList, event.review);
                notifyItemRangeChanged(reviewList.size(), event.positionInList - previousListSize + 1);
            } else {
                reviewList.add(event.positionInList, event.review);
                notifyItemChanged(event.positionInList);
            }
        }
    }

    @Subscribe
    public void onListSizeReady(MovieDataManagerImpl.ListSizeReadyEvent event) {
        if (event.listType == MovieDataManager.ListType.REVIEW) {
            int previousListSize = reviewList.size();
            reviewList.ensureCapacity(event.size);
            notifyItemRangeChanged(previousListSize, reviewList.size() - previousListSize);
        }
    }
}