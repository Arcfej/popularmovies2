package com.success.android.popularmovies2.views.details;

import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.success.android.popularmovies2.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsViewImpl implements DetailsView {

    private final View rootView;
    @BindView(R.id.iv_background)
    ImageView background;
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.tv_release_date)
    TextView releaseDate;
    @BindView(R.id.tv_plot_synopsis)
    TextView plotSynopsis;
    @BindView(R.id.tv_vote_average)
    TextView voteAverage;

    private Resources resources;

    public DetailsViewImpl(LayoutInflater inflater, ViewGroup container, Resources resources) {
        rootView = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, rootView);

        this.resources = resources;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void setTitle(String title) {
        if (title != null) {
            this.title.setText(title);
        }
    }

    @Override
    public void setReleaseDate(String releaseDate) {
        if (releaseDate != null) {
            this.releaseDate.setText(releaseDate);
        }
    }

    @Override
    public void setPlotSynopsis(String plotSynopsis) {
        if (plotSynopsis != null) {
            this.plotSynopsis.setText(plotSynopsis);
        }
    }

    @Override
    public void setVoteAverage(String voteAverage) {
        if (voteAverage != null) {
            this.voteAverage.setText(
                    resources.getString(R.string.details_activity_vote_average, voteAverage));
        }
    }

    @Override
    public void setBackground(Drawable picture) {
        Drawable compatBackground = DrawableCompat.wrap(picture).mutate();
        DrawableCompat.setTint(
                compatBackground,
                ResourcesCompat.getColor(resources, R.color.posterBackgroundTint, null)
        );
        DrawableCompat.setTintMode(compatBackground, PorterDuff.Mode.SCREEN);
        background.setImageDrawable(picture);
    }
}