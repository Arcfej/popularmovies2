package com.success.android.popularmovies2.views.trailer_list;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.success.android.popularmovies2.GlideRequests;
import com.success.android.popularmovies2.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrailerHolder extends RecyclerView.ViewHolder implements TrailerListItem {

    @BindView(R.id.iv_trailer_thumbnail)
    ImageView thumbnail;

    private TrailerHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static TrailerHolder constructWithInflating(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_trailer, parent, false);
        return new TrailerHolder(itemView);
    }

    @Override
    public void setTrailerThumb(@NonNull GlideRequests glideRequest, @NonNull Uri thumbPath) {
        glideRequest.load(thumbPath)
                .into(thumbnail);
    }

    @Override
    public void setOnClickListener(final Uri watchUri) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TrailerClickEvent(watchUri));
            }
        });
    }

    public static class TrailerClickEvent {

        public final Uri watchUri;

        public TrailerClickEvent(Uri watchUri) {
            this.watchUri = watchUri;
        }
    }
}