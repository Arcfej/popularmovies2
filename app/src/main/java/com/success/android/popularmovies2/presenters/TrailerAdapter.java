package com.success.android.popularmovies2.presenters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.success.android.popularmovies2.GlideRequests;
import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.pojos.Trailer;
import com.success.android.popularmovies2.views.BaseView;
import com.success.android.popularmovies2.views.dropdown_list.DropdownListView;
import com.success.android.popularmovies2.views.trailer_list.TrailerHolder;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerHolder> implements BasePresenter {

    @Nullable
    private DropdownListView dropdownListView;
    private MovieDataManager dataManager;
    private GlideRequests glide;

    private final int movieId;
    private ArrayList<Trailer> trailerList = new ArrayList<>(0);

    public TrailerAdapter(MovieDataManager dataManager, GlideRequests glide, int movieId) {
        this.dataManager = dataManager;
        this.glide = glide;
        this.movieId = movieId;
    }

    @Override
    public void attachView(BaseView view) {
        try {
            this.dropdownListView = (DropdownListView) view;
        } catch (ClassCastException e) {
            throw new ClassCastException("You must attach a DropdownListView to TrailerAdapter");
        }

        dataManager.requestTrailerList(movieId);
    }

    @Override
    public Bundle getState() {
        return null;
    }

    private boolean hasListView() {
        return dropdownListView != null;
    }

    @NonNull
    @Override
    public TrailerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (!hasListView()) {
            throw new NullPointerException("You must attach a view to the presenter");
        }
        return TrailerHolder.constructWithInflating(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull TrailerHolder holder, int position) {
        Trailer trailer = trailerList.get(position);
        if (trailer != null) {
            holder.setTrailerThumb(glide, trailer.getThumbUri());
            holder.setOnClickListener(trailer.getWatchUri());
        }
    }

    @Override
    public int getItemCount() {
        return trailerList.size();
    }

    @Subscribe
    public void onTrailerReady(MovieDataManagerImpl.TrailerReadyEvent event) {
        if (movieId == event.movieId) {
            if (event.positionInList >= trailerList.size()) {
                int previousListSize = trailerList.size();
                trailerList.ensureCapacity(event.positionInList + 1);
                trailerList.add(event.positionInList, event.trailer);
                notifyItemRangeChanged(trailerList.size(), event.positionInList - previousListSize + 1);
            } else {
                trailerList.add(event.positionInList, event.trailer);
                notifyItemChanged(event.positionInList);
            }
        }
    }

    @Subscribe
    public void onListSizeReady(MovieDataManagerImpl.ListSizeReadyEvent event) {
        if (event.listType == MovieDataManager.ListType.TRAILER) {
            int previousListSize = trailerList.size();
            trailerList.ensureCapacity(event.size);
            notifyItemRangeChanged(previousListSize, trailerList.size() - previousListSize);
        }
    }
}