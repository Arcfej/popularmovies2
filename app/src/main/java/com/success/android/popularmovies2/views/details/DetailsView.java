package com.success.android.popularmovies2.views.details;

import android.graphics.drawable.Drawable;

import com.success.android.popularmovies2.views.BaseView;

public interface DetailsView extends BaseView {

    void setTitle(String title);

    void setReleaseDate(String releaseDate);

    void setPlotSynopsis(String synopsis);

    void setVoteAverage(String voteAverage);

    void setBackground(Drawable background);
}
