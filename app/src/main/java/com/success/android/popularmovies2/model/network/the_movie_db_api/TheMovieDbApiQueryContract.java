package com.success.android.popularmovies2.model.network.the_movie_db_api;

import android.net.Uri;

/**
 * Created by Noki 2 on 2018. 03. 18..
 */

public class TheMovieDbApiQueryContract {

    public static final String QUERY_API_KEY = "api_key";
    public static class MovieEntry {

        public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";

        public static final String PATH_MOVIE_POPULAR = "popular";
        public static final String PATH_MOVIE_TOP_RATED = "top_rated";

        public static final String QUERY_PAGE = "page";

        private MovieEntry() {

        }

        public static Uri buildMostPopularMoviesPathUri() {
            return Uri.parse(BASE_URL)
                    .buildUpon()
                    .appendEncodedPath(PATH_MOVIE_POPULAR)
                    .build();
        }

        public static Uri buildHighestRatedMoviesPathUri() {
            return Uri.parse(BASE_URL)
                    .buildUpon()
                    .appendEncodedPath(PATH_MOVIE_TOP_RATED)
                    .build();
        }
    }

    public static class ImagesEntry {

        public static final String BASE_URL = "http://image.tmdb.org/t/p/";

        public static final String PATH_POSTER_SIZE = "w185";

        private ImagesEntry() {

        }

        public static Uri buildMovieImageDownloadUri(String posterPath) {
            return Uri.parse(BASE_URL)
                    .buildUpon()
                    .appendPath(PATH_POSTER_SIZE)
                    .appendEncodedPath(posterPath)
                    .build();
        }
    }

    public static class ReviewsEntry {

        public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";

        public static final String PATH_REVIEWS = "reviews";

        public static Uri buildReviewsDownloadUri(int movieId) {
            return Uri.parse(BASE_URL)
                    .buildUpon()
                    .appendPath(String.valueOf(movieId))
                    .appendPath(PATH_REVIEWS)
                    .build();
        }

        private ReviewsEntry() {

        }
    }

    public static class VideosEntry {

        public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";

        public static final String PATH_VIDEOS = "videos";

        public static Uri buildVideosDownloadUri(int movieId) {
            return Uri.parse(BASE_URL)
                    .buildUpon()
                    .appendPath(String.valueOf(movieId))
                    .appendPath(PATH_VIDEOS)
                    .build();
        }

        private VideosEntry() {

        }
    }

    private TheMovieDbApiQueryContract() {

    }
}
