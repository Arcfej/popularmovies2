package com.success.android.popularmovies2.model;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.util.LruCache;

import com.example.myapp.MyEventBusIndex;
import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.model.database.MoviesDbContract;
import com.success.android.popularmovies2.model.database.MoviesDbCrudHandler;
import com.success.android.popularmovies2.model.network.the_movie_db_api.TheMovieDbApiHelper;
import com.success.android.popularmovies2.model.network.the_movie_db_api.TheMovieDbApiHelperImpl;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.pojos.Review;
import com.success.android.popularmovies2.pojos.Trailer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MovieDataManagerImpl implements MovieDataManager {

    // Singleton object
    private static MovieDataManagerImpl INSTANCE;

    private final EventBus dataManagerBus;

    private final ContentResolver contentResolver;
    private final TheMovieDbApiHelper theMovieDbApiHelper;
    private final AsyncQueryHandler databaseHandler;

    private MovieDataManagerImpl(ContentResolver contentResolver, Resources resources) {
        dataManagerBus = EventBus.builder().addIndex(new MyEventBusIndex()).build();

        this.contentResolver = contentResolver;
        theMovieDbApiHelper = TheMovieDbApiHelperImpl.getInstance(
                resources.getString(R.string.api_key_v3_auth),
                dataManagerBus
        );
        databaseHandler = new MoviesDbCrudHandler(contentResolver, dataManagerBus);
    }

    public static synchronized MovieDataManagerImpl getInstance(ContentResolver contentResolver,
                                                                Resources resources) {
        if (INSTANCE == null) {
            INSTANCE = new MovieDataManagerImpl(contentResolver, resources);
        }
        return INSTANCE;
    }

    @Override
    public void subscribeToEvents() {
        dataManagerBus.register(this);
    }

    @Override
    public void unsubscribeFromEvents() {
        dataManagerBus.unregister(this);
    }

    private List<Movie> mostPopularMovies = new ArrayList<>();
    private List<Movie> highestRatedMovies = new ArrayList<>();
    private Set<Movie> favouriteMovies = new HashSet<>();

    @Override
    public void requestMovieList(ListType listType) {
        List<Movie> movieList;
        switch (listType) {
            case MOST_POPULAR:
                movieList = mostPopularMovies;
                break;
            case TOP_RATED:
                movieList = highestRatedMovies;
                break;
            case FAVOURITE:
                movieList = new ArrayList<>(favouriteMovies);
                break;
            default:
                movieList = new ArrayList<>(0);
        }
        if (movieList.size() == 0 || listType == ListType.FAVOURITE) {
            getMovieList(listType);
        } else {
            postMovieList(listType, movieList);
        }
    }

    private void postMovieList(ListType listType, List<Movie> movieList) {
        int listSize = movieList.size();
        EventBus.getDefault().post(new ListSizeReadyEvent(listType, listSize));
        for (int i = 0; i < listSize; i++) {
            EventBus.getDefault().post(new MovieReadyEvent(listType, movieList.get(i), i));
        }
    }

    private static final int TOKEN_FULL_FAVOURITE_LIST = 23;

    private void getMovieList(ListType listType) {
        switch (listType) {
            case MOST_POPULAR:
                theMovieDbApiHelper.downloadMostPopularMoviesPage(1);
                break;
            case TOP_RATED:
                theMovieDbApiHelper.downloadHighestRatedMoviesPage(1);
                break;
            case FAVOURITE:
                databaseHandler.startQuery(
                        TOKEN_FULL_FAVOURITE_LIST,
                        null,
                        MoviesDbContract.FavouriteMoviesEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                );
                break;
        }
    }

    private LruCache<Integer, ArrayList<Review>> reviewCache = new LruCache<>(40);

    @Override
    public void requestReviewList(int movieId) {
        ArrayList<Review> reviews = reviewCache.get(movieId);
        if (reviews == null) {
            theMovieDbApiHelper.downloadReviewsPage(movieId, 1);
        } else {
            postReviewList(movieId, reviews);
        }
    }

    private void postReviewList(int movieId, ArrayList<Review> reviews) {
        EventBus.getDefault().post(new ListSizeReadyEvent(ListType.REVIEW, reviews.size()));
        for (int i = 0; i < reviews.size(); i++) {
            Review review = reviews.get(i);
            EventBus.getDefault().post(new ReviewReadyEvent(movieId, review, i));
        }
    }

    private LruCache<Integer, ArrayList<Trailer>> trailerCache = new LruCache<>(40);

    @Override
    public void requestTrailerList(int movieId) {
        ArrayList<Trailer> trailers = trailerCache.get(movieId);
        if (trailers == null) {
            theMovieDbApiHelper.downloadTrailers(movieId);
        } else {
            postTrailerList(movieId, trailers);
        }
    }

    private void postTrailerList(int movieId, ArrayList<Trailer> trailers) {
        EventBus.getDefault().post(new ListSizeReadyEvent(ListType.TRAILER, trailers.size()));
        for (int i = 0; i < trailers.size(); i++) {
            Trailer trailer = trailers.get(i);
            EventBus.getDefault().post(new TrailerReadyEvent(movieId, trailer, i));
        }
    }

    private static final int TOKEN_IS_FAVOURITE = 28;

    @Override
    public void isFavourite(int movieId) {
        Uri requestUri = ContentUris.withAppendedId(
                MoviesDbContract.FavouriteMoviesEntry.CONTENT_URI, movieId
        );

        databaseHandler.startQuery(
                TOKEN_IS_FAVOURITE,
                null,
                requestUri,
                new String[]{MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID},
                null,
                null,
                null
        );
    }

    @Override
    public void addMovieToFavourite(Movie movie) {
        Uri requestUri = MoviesDbContract.FavouriteMoviesEntry.CONTENT_URI;

        ContentValues values = new ContentValues();
        int movieId = movie.getMovieId();
        if (movieId < 0) {
            // TODO on error?
            return;
        }
        String title = movie.getTitle();
        if (title == null) {
            return;
        }
        values.put(MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID, movieId);
        values.put(MoviesDbContract.FavouriteMoviesEntry.COLUMN_TITLE, title);
        values.put(MoviesDbContract.FavouriteMoviesEntry.COLUMN_SYNOPSIS, movie.getPlotSynopsis());
        values.put(MoviesDbContract.FavouriteMoviesEntry.COLUMN_RELEASE_DATE, movie.getReleaseDate());
        values.put(MoviesDbContract.FavouriteMoviesEntry.COLUMN_POSTER_PATH, movie.getPosterPath().toString());
        values.put(MoviesDbContract.FavouriteMoviesEntry.COLUMN_VOTE_AVERAGE, movie.getVoteAverage());

        databaseHandler.startInsert(movie.getMovieId(), null, requestUri, values);
    }

    @Override
    public void removeMovieFromFavourites(int movieId) {
        Uri requestUri = ContentUris.withAppendedId(
                MoviesDbContract.FavouriteMoviesEntry.CONTENT_URI, movieId);

        databaseHandler.startDelete(movieId, null, requestUri, null, null);
    }

    @Subscribe
    public void onMoviePageDownloaded(TheMovieDbApiHelperImpl.MoviePageDownloadedEvent event) {
        switch (event.listType) {
            case MOST_POPULAR:
                mostPopularMovies = event.movieList;
                break;
            case TOP_RATED:
                highestRatedMovies = event.movieList;
                break;
            default:
                return;
        }
        postMovieList(event.listType, event.movieList);
    }

    @Subscribe
    public void onReviewPageDownloaded(TheMovieDbApiHelperImpl.ReviewPageDownloadedEvent event) {
        ArrayList<Review> reviews;
        try {
            reviews = (ArrayList<Review>) event.reviewList;
        } catch (ClassCastException e) {
            throw new ClassCastException("There should be an ArrayList of reviews");
        }
        reviewCache.put(event.movieId, reviews);
        postReviewList(event.movieId, reviews);
    }

    @Subscribe
    public void onTrailersDownloaded(TheMovieDbApiHelperImpl.TrailersDownloadedEvent event) {
        ArrayList<Trailer> trailers;
        try {
            trailers = (ArrayList<Trailer>) event.trailerList;
        } catch (ClassCastException e) {
            throw new ClassCastException("There should be an ArrayList of trailers");
        }
        trailerCache.put(event.movieId, trailers);
        postTrailerList(event.movieId, trailers);
    }

    @Subscribe
    public void onQueryCompleted(MoviesDbCrudHandler.QueryCompletedEvent event) {
        switch (event.token) {
            case TOKEN_FULL_FAVOURITE_LIST:
                favouriteMovies = new HashSet<>(cursorToMovieList(event.cursor));
                int positionInList = 0;
                for (Movie movie : favouriteMovies) {
                    EventBus.getDefault().post(
                            new MovieReadyEvent(ListType.FAVOURITE, movie, positionInList)
                    );
                    positionInList++;
                }
                break;
            case TOKEN_IS_FAVOURITE:
                if (event.cursor == null) {
                    return;
                }
                event.cursor.moveToFirst();
                int movieIdColumnIndex = event.cursor.getColumnIndex(
                        MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID
                );
                int movieId = event.cursor.getInt(movieIdColumnIndex);

                EventBus.getDefault().post(new MovieFavouriteStateChanged(movieId, true));
                event.cursor.close();
            case TOKEN_MOVIE_INSERTED:
                event.cursor.moveToFirst();
                favouriteMovies.add(cursorToMovie(event.cursor));
                event.cursor.close();

                EventBus.getDefault().post(
                        new ListSizeReadyEvent(ListType.FAVOURITE, favouriteMovies.size())
                );
        }
    }

    private List<Movie> cursorToMovieList(Cursor cursor) {
        if (cursor == null || cursor.getCount() == 0) {
            return new ArrayList<>(0);
        }
        List<Movie> movieList = new ArrayList<>(cursor.getCount());
        while (cursor.moveToNext()) {
            movieList.add(cursorToMovie(cursor));
        }
        cursor.close();
        return movieList;
    }

    private Movie cursorToMovie(Cursor c) {
        String posterPath =
                c.getString(c.getColumnIndex(MoviesDbContract.FavouriteMoviesEntry.COLUMN_POSTER_PATH));
        return new Movie(
                c.getInt(c.getColumnIndex(MoviesDbContract.FavouriteMoviesEntry.COLUMN_MOVIE_ID)),
                c.getString(c.getColumnIndex(MoviesDbContract.FavouriteMoviesEntry.COLUMN_TITLE)),
                c.getString(c.getColumnIndex(MoviesDbContract.FavouriteMoviesEntry.COLUMN_SYNOPSIS)),
                c.getLong(c.getColumnIndex(MoviesDbContract.FavouriteMoviesEntry.COLUMN_RELEASE_DATE)),
                Uri.parse(posterPath),
                c.getDouble(c.getColumnIndex(MoviesDbContract.FavouriteMoviesEntry.COLUMN_VOTE_AVERAGE)));
    }

    private static final int TOKEN_MOVIE_INSERTED = 29;

    @Subscribe
    public void onInsertionCompleted(MoviesDbCrudHandler.InsertionCompletedEvent event) {
        int movieId = (int) ContentUris.parseId(event.uri);
        if (movieId != -1) {
            EventBus.getDefault().post(new MovieFavouriteStateChanged(event.token, true));
            databaseHandler.startQuery(TOKEN_MOVIE_INSERTED, null, event.uri,
                    null, null, null, null
            );
        }
    }

    @Subscribe
    public void onDeletionCompleted(MoviesDbCrudHandler.DeletionCompletedEvent event) {
        if (event.deletedRows == 1) {
            EventBus.getDefault().post(new MovieFavouriteStateChanged(event.token, false));
        }
        getMovieList(ListType.FAVOURITE);
    }

    public static class MovieReadyEvent {

        public final ListType listType;
        public final Movie movie;
        public final int positionInList;

        public MovieReadyEvent(ListType listType, Movie movie, int positionInList) {
            this.listType = listType;
            this.movie = movie;
            this.positionInList = positionInList;
        }
    }

    public class ReviewReadyEvent {

        public final int movieId;
        public final Review review;
        public final int positionInList;

        public ReviewReadyEvent(int movieId, Review review, int positionInList) {
            this.movieId = movieId;
            this.review = review;
            this.positionInList = positionInList;
        }
    }

    public class TrailerReadyEvent {

        public final int movieId;
        public final Trailer trailer;
        public final int positionInList;

        public TrailerReadyEvent(int movieId, Trailer trailer, int positionInList) {
            this.movieId = movieId;
            this.trailer = trailer;
            this.positionInList = positionInList;
        }
    }

    public static class ListSizeReadyEvent {

        public final ListType listType;
        public final int size;

        public ListSizeReadyEvent(ListType listType, int size) {
            this.listType = listType;
            this.size = size;
        }
    }

    public static class ListTypeChangedEvent {

        public final ListType newListType;
        public ListTypeChangedEvent(ListType newListType) {
            this.newListType = newListType;
        }
    }

    public static class MovieFavouriteStateChanged {

        public final int movieId;
        public final boolean isFavourite;

        public MovieFavouriteStateChanged(int movieId, boolean isFavourite) {
            this.movieId = movieId;
            this.isFavourite = isFavourite;
        }
    }
}