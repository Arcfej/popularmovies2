package com.success.android.popularmovies2.pojos;

import android.net.Uri;

public class Trailer {

    private int movieId;
    private String name;
    private Uri watchUri;
    private Uri thumbUri;

    public Trailer(int movieId, String name, Uri watchUri, Uri thumbUri) {
        this.movieId = movieId;
        this.name = name;
        this.watchUri = watchUri;
        this.thumbUri = thumbUri;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getName() {
        return name;
    }

    public Uri getWatchUri() {
        return watchUri;
    }

    public Uri getThumbUri() {
        return thumbUri;
    }
}
