package com.success.android.popularmovies2.model.network.youtube_api;

import android.net.Uri;

public class YouTubeApiContract {

    public static final String BASE_WATCH_URL = "https://www.youtube.com/";

    public static final String PATH_WATCH = "watch";

    public static final String QUERY_VIDEO_KEY = "v";

    public static Uri buildWatchUri(String videoId) {
        return Uri.parse(BASE_WATCH_URL)
                .buildUpon()
                .appendPath(PATH_WATCH)
                .appendQueryParameter(QUERY_VIDEO_KEY, videoId)
                .build();
    }

    public static final String BASE_IMAGE_URL = "https://img.youtube.com/vi/";

    public static final String PATH_DEFAULT = "default.jpg";

    public static Uri buildThumbnailUri(String videoId) {
        return Uri.parse(BASE_IMAGE_URL)
                .buildUpon()
                .appendPath(videoId)
                .appendPath(PATH_DEFAULT)
                .build();
    }
}
