package com.success.android.popularmovies2.views.movie_list;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.success.android.popularmovies2.GlideRequests;
import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.pojos.Movie;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviePosterHolder extends RecyclerView.ViewHolder implements MovieListItem {

    @BindView(R.id.iv_movie_poster)
    ImageView moviePoster;

    private MoviePosterHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static MoviePosterHolder constructWithInflating(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_movie_poster, parent, false);
        return new MoviePosterHolder(itemView);
    }

    @Override
    public void setPoster(@NonNull GlideRequests glideRequest, @NonNull Uri posterPath) {
        moviePoster.layout(0, 0, 0, 0);
        glideRequest.load(posterPath)
                .placeholder(R.drawable.movie_poster_placeholder)
                .into(moviePoster);
    }

    @Override
    public void setOnClickListener(final Movie movie) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PosterClickEvent(movie));
            }
        });
    }

    public static class PosterClickEvent {

        public final Movie movie;

        public PosterClickEvent(Movie movie) {
            this.movie = movie;
        }
    }
}