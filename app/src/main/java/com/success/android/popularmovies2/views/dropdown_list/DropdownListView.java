package com.success.android.popularmovies2.views.dropdown_list;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.views.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DropdownListView implements BaseView {

    private static final String KEY_IS_LIST_VISIBLE = "is_visible";

    private final View rootView;
    @BindView(R.id.tv_dropdown_head)
    TextView dropdownHead;
    @BindView(R.id.rv_dropdown_list)
    RecyclerView dropdownList;

    private Context applicationContext;

    protected RecyclerView.LayoutManager layoutManager;
    private boolean isListVisible = false;

    public DropdownListView(Context applicationContext, LayoutInflater inflater,
                            ViewGroup container, RecyclerView.Adapter presenter, Bundle viewState,
                            String headTitle, int orientation) {
        rootView = inflater.inflate(R.layout.fragment_dropdown_list, container, false);
        ButterKnife.bind(this, rootView);

        this.applicationContext = applicationContext;

        dropdownHead.setText(headTitle);

        if (orientation == LinearLayoutManager.HORIZONTAL ||
                orientation == LinearLayoutManager.VERTICAL) {
            layoutManager = new LinearLayoutManager(applicationContext,
                    orientation, false);
        }

        dropdownList.setLayoutManager(layoutManager);
        dropdownList.setAdapter(presenter);

        if (viewState != null) {
            isListVisible = viewState.getBoolean(KEY_IS_LIST_VISIBLE, false);
        }
        dropdownList.setVisibility((isListVisible) ? View.VISIBLE : View.GONE);

        dropdownHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListHeadClick();
            }
        });
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle state = new Bundle();
        state.putBoolean(KEY_IS_LIST_VISIBLE, isListVisible);
        return state;
    }

    private void onListHeadClick() {
        if (isListVisible) {
            hideList();
        } else {
            showList();
        }
    }

    private void showList() {
        dropdownList.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            dropdownHead.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_drop_up_white_24dp,
                    0
            );
        } else {
            dropdownHead.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_drop_up_white_24dp,
                    0
            );
        }
        isListVisible = true;
    }

    private void hideList() {
        dropdownList.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            dropdownHead.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_drop_down_white_24dp,
                    0
            );
        } else {
            dropdownHead.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_drop_down_white_24dp,
                    0
            );
        }
        isListVisible = false;
    }
}