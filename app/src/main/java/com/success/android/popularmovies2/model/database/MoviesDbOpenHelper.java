package com.success.android.popularmovies2.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.success.android.popularmovies2.model.database.MoviesDbContract.FavouriteMoviesEntry;

public class MoviesDbOpenHelper extends SQLiteOpenHelper {

    private static final String MOVIES_DATABASE_NAME = "movies.db";
    private static final int MOVIES_DATABASE_VERSION = 1;

    public MoviesDbOpenHelper(Context context) {
        super(context, MOVIES_DATABASE_NAME, null, MOVIES_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + FavouriteMoviesEntry.TABLE_NAME + " ("
                + FavouriteMoviesEntry.COLUMN_MOVIE_ID + " INTEGER PRIMARY KEY, "
                + FavouriteMoviesEntry.COLUMN_TITLE + " TEXT NOT NULL, "
                + FavouriteMoviesEntry.COLUMN_SYNOPSIS + " TEXT, "
                + FavouriteMoviesEntry.COLUMN_RELEASE_DATE + " BIGINT, "
                + FavouriteMoviesEntry.COLUMN_POSTER_PATH + " TEXT, "
                + FavouriteMoviesEntry.COLUMN_VOTE_AVERAGE + " FLOAT" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            // Implement later
            default:
                db.execSQL("DROP TABLE IF EXISTS " + FavouriteMoviesEntry.TABLE_NAME);
                onCreate(db);
        }
    }
}