package com.success.android.popularmovies2.views.movie_list;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.success.android.popularmovies2.R;
import com.success.android.popularmovies2.views.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListView implements BaseView {

    private final View rootView;
    @BindView(R.id.rv_movie_list)
    RecyclerView list;
    @BindView(R.id.tv_empty_list)
    TextView emptyList;

    private RecyclerView.Adapter presenter;
    private RecyclerView.LayoutManager layoutManager;

    public MovieListView(LayoutInflater inflater, ViewGroup container,
                         RecyclerView.Adapter presenter,
                         WindowManager windowManager) {
        rootView = inflater.inflate(R.layout.fragment_full_size_list, container, false);
        ButterKnife.bind(this, rootView);

        int rotation = windowManager.getDefaultDisplay().getRotation();
        int spanCount = (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) ? 2 : 4;
        layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        list.setLayoutManager(layoutManager);

        this.presenter = presenter;
        list.setAdapter(presenter);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}