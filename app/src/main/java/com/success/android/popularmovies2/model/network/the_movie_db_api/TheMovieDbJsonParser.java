package com.success.android.popularmovies2.model.network.the_movie_db_api;

import android.net.Uri;

import com.success.android.popularmovies2.model.network.youtube_api.YouTubeApiContract;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.pojos.Review;
import com.success.android.popularmovies2.pojos.Trailer;
import com.success.android.popularmovies2.utilities.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noki 2 on 2018. 03. 18..
 */

public class TheMovieDbJsonParser {

    private TheMovieDbJsonParser() {

    }

    private static final String MOVIES_PAGE_RESULTS_KEY = "results";
    private static final String MOVIE_ID_KEY = "id";
    private static final String MOVIE_TITLE_KEY = "title";
    private static final String MOVIE_PLOT_SYNOPSIS_KEY = "overview";
    private static final String MOVIE_RELEASE_DATE_KEY = "release_date";
    private static final String MOVIE_POSTER_PATH_KEY = "poster_path";
    private static final String MOVIE_VOTE_AVERAGE_KEY = "vote_average";
    
    public static List<Movie> parseMoviesPage(JSONObject page) {
        List<Movie> movieList = new ArrayList<>();
        try {
            JSONArray results = page.getJSONArray(MOVIES_PAGE_RESULTS_KEY);
            for (int i = 0; i < results.length(); i++) {
                JSONObject item = results.getJSONObject(i);
                int movieId = item.optInt(MOVIE_ID_KEY);
                String title = item.optString(MOVIE_TITLE_KEY);
                String plotSynopsis = item.optString(MOVIE_PLOT_SYNOPSIS_KEY);
                String releaseDate = item.optString(MOVIE_RELEASE_DATE_KEY);
                Uri posterPath = TheMovieDbApiQueryContract.ImagesEntry.buildMovieImageDownloadUri(
                        item.optString(MOVIE_POSTER_PATH_KEY));
                double voteAverage = item.optDouble(MOVIE_VOTE_AVERAGE_KEY);
                Movie movie = new Movie(movieId, title, plotSynopsis,
                        DateUtils.getTimeInMillisFromThemoviedbResponse(releaseDate), posterPath,
                        voteAverage);
                movieList.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return movieList;
    }

    private static final String MOVIES_PAGE_NUMBER_KEY = "page";

    public static int fetchMoviesPageNumber(JSONObject page) {
        return page.optInt(MOVIES_PAGE_NUMBER_KEY);
    }

    private static final String REVIEWS_PAGE_MOVIE_ID_KEY = "id";
    private static final String REVIEWS_PAGE_RESULTS_KEY = "results";
    private static final String REVIEW_ID_KEY = "id";
    private static final String REVIEW_AUTHOR_KEY = "author";
    private static final String REVIEW_CONTENT_KEY = "content";
    private static final String REVIEW_URL_KEY = "url";

    public static List<Review> parseReviewsPage(JSONObject page) {
        List<Review> reviewList = new ArrayList<>();
        try {
            int movieId = page.optInt(REVIEWS_PAGE_MOVIE_ID_KEY, -1);
            JSONArray results = page.getJSONArray(REVIEWS_PAGE_RESULTS_KEY);
            for (int i = 0; i < results.length(); i++) {
                JSONObject review = results.getJSONObject(i);
                String author = review.optString(REVIEW_AUTHOR_KEY);
                String content = review.optString(REVIEW_CONTENT_KEY);
                String url = review.optString(REVIEW_URL_KEY);
                reviewList.add(new Review(movieId, author, content, url));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return reviewList;
    }

    private static final String REVIEWS_PAGE_NUMBER_KEY = "page";

    public static int fetchReviewsPageNumber(JSONObject page) {
        return page.optInt(REVIEWS_PAGE_NUMBER_KEY);
    }

    private static final String VIDEOS_MOVIE_ID_KEY = "id";
    private static final String VIDEOS_RESULTS_KEY = "results";
    private static final String VIDEO_KEY_KEY = "key";
    private static final String VIDEO_NAME_KEY = "name";
    private static final String VIDEO_SITE_KEY = "site";
    private static final String VIDEO_TYPE_KEY = "type";
    private static final String VIDEO_TYPE_TRAILER = "Trailer";
    private static final String VIDEO_SITE_YOUTUBE = "YouTube";

    public static List<Trailer> parseTrailers(JSONObject object) {
        List<Trailer> trailerList = new ArrayList<>();
        try {
            int movieId = object.optInt(VIDEOS_MOVIE_ID_KEY, -1);
            JSONArray results = object.getJSONArray(VIDEOS_RESULTS_KEY);
            for (int i = 0; i < results.length(); i++) {
                JSONObject video = results.getJSONObject(i);
                if (video.optString(VIDEO_TYPE_KEY).equals(VIDEO_TYPE_TRAILER) &&
                        video.optString(VIDEO_SITE_KEY).equals(VIDEO_SITE_YOUTUBE)) {
                    String name = video.optString(VIDEO_NAME_KEY);
                    String videoId = video.optString(VIDEO_KEY_KEY);
                    Uri watchUri = YouTubeApiContract.buildWatchUri(videoId);
                    Uri thumbUri = YouTubeApiContract.buildThumbnailUri(videoId);
                    trailerList.add(new Trailer(movieId, name, watchUri, thumbUri));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return trailerList;
    }
}
