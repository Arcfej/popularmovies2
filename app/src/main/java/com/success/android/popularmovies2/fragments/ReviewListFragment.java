package com.success.android.popularmovies2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.popularmovies2.model.MovieDataManager;
import com.success.android.popularmovies2.model.MovieDataManagerImpl;
import com.success.android.popularmovies2.pojos.Movie;
import com.success.android.popularmovies2.presenters.ReviewAdapter;
import com.success.android.popularmovies2.views.review_list.ReviewListView;

import org.greenrobot.eventbus.EventBus;

public class ReviewListFragment extends Fragment {

    private static final String KEY_VIEW_STATE = "view_state";

    private Bundle viewState;

    private Movie movie;

    private Context applicationContext;

    private ReviewAdapter presenter;
    private MovieDataManager dataManager;
    private ReviewListView view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataManager = MovieDataManagerImpl.getInstance(context.getContentResolver(), getResources());
        applicationContext = context.getApplicationContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments == null || !arguments.containsKey(DetailsFragment.KEY_MOVIE)) {
            throw new IllegalArgumentException("The DetailsFragment must contain a Movie in it's arguments");
        } else {
            movie = arguments.getParcelable(DetailsFragment.KEY_MOVIE);
        }

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_VIEW_STATE)) {
            viewState = savedInstanceState.getBundle(KEY_VIEW_STATE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new ReviewAdapter(dataManager, movie.getMovieId());

        view = new ReviewListView(applicationContext, inflater, container, presenter, viewState);

        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(presenter);
        presenter.attachView(view);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(KEY_VIEW_STATE, view.getViewState());
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(presenter);
    }
}