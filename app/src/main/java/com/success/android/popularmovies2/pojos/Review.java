package com.success.android.popularmovies2.pojos;

public class Review {

    private int movieId;
    private String author;
    private String content;
    private String url;

    public Review(int movieId, String author, String content, String url) {
        this.movieId = movieId;
        this.author = author;
        this.content = content;
        this.url = url;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public String getUrl() {
        return url;
    }
}
