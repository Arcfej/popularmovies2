Used third party libraries:


Glide: https://github.com/bumptech/glide/blob/master/LICENSE

Fast Android Networking: https://github.com/amitshekhariitbhu/Fast-Android-Networking/blob/master/LICENSE

EventBus: https://github.com/greenrobot/EventBus/blob/master/LICENSE

Butter Knife: https://github.com/JakeWharton/butterknife/blob/master/LICENSE.txt